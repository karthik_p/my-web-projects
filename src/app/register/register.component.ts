import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  registerUser:any;
  countries:Array<any>;

  constructor(private datasvc:DataService ) { 
    this.registerUser = {
      firstname:"",
      lastname:"",
      username:"",
      password:"",
      email:"",
      gender:"",
      country:"",
      agree:false

    }
    this.countries= [{name:"India",code:"IN"},
    {name:"Australia",code:"AUS"},
    {name:"United States",code:"USA"}];
  }
  register(){
    console.log(this.registerUser);
  }
}
