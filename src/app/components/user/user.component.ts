import { Component, OnInit } from '@angular/core';
import{DataService} from '../../services/data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  name:string;
  age:number;
  email:string;
  address:Address;
  hobbies:string[];
  hello:any;
  posts:Posts[];
  isEdit:boolean=false;
  constructor(private dataService:DataService) {

   }

  ngOnInit() {
    this.name = 'karthik';
    this.age=25;
    this.email='karthik.ponugoti123@gmail.com';
    this.address={
        street:'R.T.C COLONY',
        city:'karimnagar',
        state:'ts',
    }
    this.hobbies=['write code', 'watch code','listen to music'];
    this.hello='hello';
    this.dataService.getPosts().subscribe((posts)=>{
      //console.log(posts);
      this.posts=posts;
    });
  }

  onClick(){
    this.name='kalyan';
    this.age=21;
    this.email='kalyan.ponugoti@gmail.com';
    this.hobbies.push('playing cricket');
  }
  addHobby(hobby){
    this.hobbies.unshift(hobby);
    return false;
  }
  deleteHobby(hobby){
    for(let i=0;i<this.hobbies.length;i++){
      if(this.hobbies[i] == hobby){
        this.hobbies.splice(i,1);
      }
    }
  }
  toggleEdit(){
    this.isEdit=!this.isEdit;
  }
}

interface Address{
    street:string,
    city:string,
    state:string;
}

interface Posts{
  id:number;
  userId:number;
  title:string;
  body:string;
}