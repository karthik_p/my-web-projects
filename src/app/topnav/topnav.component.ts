import { Component } from '@angular/core';
@Component({
    selector: 'travel-topnav',
    templateUrl: './topnav.component.html'
})
export class TopNavComponent {
    TitleAndImage: any;
    CenterTabs: any;
    RightTabs: any;

    constructor() {
        this.TitleAndImage = {
            title: "Travel Guru",
            image: "https://www.travelguru.com/travelguru/resources/beetle/images/tg/travelguru-homestay-logo-199x52.png"
        };
        this.CenterTabs = [{ display: "Home", path: "home",icon:"fa fa-home" },
        { display: "Hotels", path: "hotels" },
        { display: "Payment", path: 'payment' }];
        
        this.RightTabs = [{ display: "Login", path: 'login', visible: true },
        { display: "Register", path: 'register', visible: true }];
    }
}