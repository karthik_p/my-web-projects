import {Component} from '@angular/core';
@Component({
    selector:'travel-footer',
    templateUrl:'./footer.component.html'
})
export class FooterComponent{
   footer:any;
    constructor(){
        this.footer ={
            text:"Copyrights",
            year: new Date().getFullYear()
        }
   }
}