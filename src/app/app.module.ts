import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule,Route } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TopNavComponent } from './topnav/topnav.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { HotelsComponent } from './hotels/hotels.component';
import { HoteldetailComponent } from './hoteldetail/hoteldetail.component';
import { PaymentComponent } from './payment/payment.component';
import { DataService } from './services/data.service';
import { UserComponent } from './components/user/user.component';
import { AboutComponent } from './components/about/about.component';

// const appRoutes: Routes = [
// {path:'', component:UserComponent  },
// {path:'about', component:AboutComponent  }
// ];

@NgModule({ 
  declarations: [
    AppComponent,
    TopNavComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    HotelsComponent,
    HoteldetailComponent,
    PaymentComponent,
    UserComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    // RouterModule.forRoot(appRoutes)
    RouterModule.forRoot([{ path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent }])
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
